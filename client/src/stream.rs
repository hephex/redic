use proto;

use std::io;
use super::{Client, Error as ClientError, PubSub};


pub struct StreamClient<S> {
    stream: S,
}

impl<S> StreamClient<S> {
    pub fn new(stream: S) -> StreamClient<S> {
        StreamClient {
            stream: stream,
        }
    }
}


impl<S> Client for StreamClient<S>
    where S: io::Read + io::Write
{
    type Error = io::Error;
    type PS = S;

    fn cmd<C: proto::cmd::Cmd>(&mut self, cmd: C) -> Result<C::Reply, ClientError<Self::Error>> {
        cmd.send(&mut self.stream)
            .map_err(ClientError::Send)?;

        cmd.recv(&mut io::BufReader::new(&mut self.stream))
            .map_err(ClientError::Recv)
    }

    fn pubsub(self) -> PubSub<S> {
        PubSub { stream: self.stream }
    }
}
