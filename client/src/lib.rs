extern crate redic_proto as proto;

pub mod stream;

use proto::{ReadError, Resp};
use proto::cmd::{Cmd, RecvError};
use std::fmt;
use std::io;

#[derive(Debug)]
pub enum Error<E: fmt::Debug> {
    Client(E),
    Send(io::Error),
    Recv(RecvError),
}


pub struct PubSub<S: io::Read + io::Write> {
    stream: S,
}

#[derive(Debug, PartialEq, Eq)]
pub enum PubSubMsg {
    Subscribe { channel: proto::Bulk, rem: i64 },
    Unsubscribe { channel: proto::Bulk, rem: i64 },
    Message { channel: proto::Bulk, payload: proto::Bulk },

}

fn parse_message(mut array: Vec<Resp>) -> Result<PubSubMsg, RecvError> {
    match (array.pop(), array.pop(), array.pop()) {
        (Some(Resp::Bulk(data)), Some(Resp::Bulk(channel)), Some(Resp::Bulk(kind))) => {
            match kind.as_slice() {
                b"message" => {
                    Ok(PubSubMsg::Message { channel: channel, payload: data })
                },
                _ => Err(RecvError::UnexpectedResp),
            }
        },
        (Some(Resp::Integer(rem)), Some(Resp::Bulk(channel)), Some(Resp::Bulk(kind))) => {
            match kind.as_slice() {
                b"subscribe" => {
                    Ok(PubSubMsg::Subscribe { channel: channel, rem: rem })
                },
                b"unsubscribe" => {
                    Ok(PubSubMsg::Unsubscribe { channel: channel, rem: rem })
                },
                _ => Err(RecvError::UnexpectedResp),
            }
        },
        _ => Err(RecvError::UnexpectedResp),
    }
}


impl<S: io::Read + io::Write> PubSub<S> {

    pub fn subscribe(&mut self, channels: Vec<proto::Bulk>) -> Result<(), io::Error> {
        proto::cmd::Subscribe {
            channels: channels,
        }.send(&mut self.stream)
    }

    pub fn unsubscribe(&mut self, channels: Vec<proto::Bulk>) -> Result<(), io::Error> {
        proto::cmd::Unsubscribe {
            channels: channels,
        }.send(&mut self.stream)
    }

    pub fn next_message(&mut self) -> Result<PubSubMsg, RecvError> {
        let msg = Resp::decode(&mut io::BufReader::new(&mut self.stream))
            .map_err(RecvError::Read)?;

        if let Resp::Array(a) = msg {
            parse_message(a)
        } else {
            Err(RecvError::UnexpectedResp)
        }
    }
}


pub trait Client {
    type Error: fmt::Debug;
    type PS: io::Read + io::Write;

    fn cmd<C: proto::cmd::Cmd>(&mut self, cmd: C) -> Result<C::Reply, Error<Self::Error>>;

    fn pubsub(self) -> PubSub<Self::PS>;
}
