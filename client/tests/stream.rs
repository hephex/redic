extern crate redic_client as client;
extern crate redic_proto as proto;

use client::Client;
use client::stream::StreamClient;
use std::env;
use std::net;
use std::time::Duration;


fn create_stream() -> StreamClient<net::TcpStream> {
    let addr = env::var("REDIT_ADDR").unwrap_or(String::from("localhost:6379"));
    let tcp = net::TcpStream::connect(addr).unwrap();

    StreamClient::new(tcp)
}


#[test]
fn get_nil() {
    let mut stream = create_stream();

    let res = stream.cmd(proto::cmd::Get{ key: vec!['a' as u8] }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Nil);
}


#[test]
fn get_data() {
    let mut stream = create_stream();

    stream.cmd(proto::cmd::Set{
        key: vec!['b' as u8],
        value: vec!['x' as u8],
        expiration: Some(Duration::from_secs(1)),
        exists: None,
    }).unwrap();

    let res = stream.cmd(proto::cmd::Get{ key: vec!['b' as u8] }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Bulk(vec!['x' as u8]));
}


#[test]
fn set_simple() {
    let mut stream = create_stream();

    let res = stream.cmd(proto::cmd::Set{
        key: vec!['c' as u8],
        value: vec!['x' as u8],
        expiration: Some(Duration::from_secs(1)),
        exists: None,
    }).unwrap();

    assert_eq!(res, proto::cmd::SetReply::Status(String::from("OK")));

    let res = stream.cmd(proto::cmd::Get{ key: vec!['c' as u8] }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Bulk(vec!['x' as u8]));
}


#[test]
fn set_expires_secs() {
    let mut stream = create_stream();

    let res = stream.cmd(proto::cmd::Set{
        key: vec!['d' as u8],
        value: vec!['x' as u8],
        expiration: Some(Duration::from_secs(1)),
        exists: None,
    }).unwrap();

    assert_eq!(res, proto::cmd::SetReply::Status(String::from("OK")));

    let res = stream.cmd(proto::cmd::Get{ key: vec!['d' as u8] }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Bulk(vec!['x' as u8]));

    std::thread::sleep(Duration::from_secs(1));

    let res = stream.cmd(proto::cmd::Get{ key: vec!['d' as u8] }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Nil);
}

#[test]
fn set_expires_millis() {
    let mut stream = create_stream();

    let res = stream.cmd(proto::cmd::Set{
        key: vec!['e' as u8],
        value: vec!['x' as u8],
        expiration: Some(Duration::new(1, 1_000_000)),
        exists: None,
    }).unwrap();

    assert_eq!(res, proto::cmd::SetReply::Status(String::from("OK")));

    let res = stream.cmd(proto::cmd::Get{ key: vec!['e' as u8] }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Bulk(vec!['x' as u8]));

    std::thread::sleep(Duration::from_secs(3));

    let res = stream.cmd(proto::cmd::Get{ key: vec!['e' as u8] }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Nil);
}

#[test]
fn set_expires_exists() {
    let mut stream = create_stream();

    let res = stream.cmd(proto::cmd::Set{
        key: vec!['f' as u8],
        value: vec!['x' as u8],
        expiration: None,
        exists: Some(true),
    }).unwrap();

    assert_eq!(res, proto::cmd::SetReply::Nil);

    let res = stream.cmd(proto::cmd::Set{
        key: vec!['f' as u8],
        value: vec!['x' as u8],
        expiration: None,
        exists: Some(false),
    }).unwrap();

    assert_eq!(res, proto::cmd::SetReply::Status(String::from("OK")));

    let res = stream.cmd(proto::cmd::Set{
        key: vec!['f' as u8],
        value: vec!['z' as u8],
        expiration: None,
        exists: Some(false),
    }).unwrap();

    assert_eq!(res, proto::cmd::SetReply::Nil);

    let res = stream.cmd(proto::cmd::Set{
        key: vec!['f' as u8],
        value: vec!['y' as u8],
        expiration: None,
        exists: Some(true),
    }).unwrap();

    assert_eq!(res, proto::cmd::SetReply::Status(String::from("OK")));

    let res = stream.cmd(proto::cmd::Get{ key: vec!['f' as u8] }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Bulk(vec!['y' as u8]));
}

#[test]
fn del_keys() {
    let mut stream = create_stream();

    let res = stream.cmd(proto::cmd::Del{
        keys: vec![
            vec!['g' as u8],
        ],
    }).unwrap();

    assert_eq!(res, 0);

    let res = stream.cmd(proto::cmd::Set{
        key: vec!['g' as u8, '1' as u8],
        value: vec!['x' as u8],
        expiration: None,
        exists: Some(false),
    }).unwrap();

    assert_eq!(res, proto::cmd::SetReply::Status(String::from("OK")));

    let res = stream.cmd(proto::cmd::Set{
        key: vec!['g' as u8, '2' as u8],
        value: vec!['x' as u8],
        expiration: None,
        exists: Some(false),
    }).unwrap();

    assert_eq!(res, proto::cmd::SetReply::Status(String::from("OK")));

    let res = stream.cmd(proto::cmd::Del{
        keys: vec![
            vec!['g' as u8, '1' as u8],
            vec!['g' as u8, '2' as u8],
        ],
    }).unwrap();

    assert_eq!(res, 2);
}

#[test]
fn append() {
    let mut stream = create_stream();

    let res = stream.cmd(proto::cmd::Append{
        key: vec!['h' as u8],
        value: vec!['1' as u8],
    }).unwrap();

    assert_eq!(res, 1);

    let res = stream.cmd(proto::cmd::Append{
        key: vec!['h' as u8],
        value: vec!['2' as u8, '3' as u8],
    }).unwrap();

    assert_eq!(res, 3);

    let res = stream.cmd(proto::cmd::Get{
        key: vec!['h' as u8],
    }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Bulk(vec!['1' as u8, '2' as u8, '3' as u8]));
}

#[test]
fn bitcount() {
    let mut stream = create_stream();

    stream.cmd(proto::cmd::Set{
        key: vec!['i' as u8],
        value: vec![1, 2, 4, 8],
        expiration: None,
        exists: None,
    }).unwrap();

    let res = stream.cmd(proto::cmd::BitCount{
        key: vec!['i' as u8],
        range: Some((1, 2)),
    }).unwrap();

    assert_eq!(res, 2);
}

#[test]
fn exists() {
    let mut stream = create_stream();

    stream.cmd(proto::cmd::Set{
        key: vec!['l' as u8],
        value: vec![1],
        expiration: None,
        exists: None,
    }).unwrap();

    let res = stream.cmd(proto::cmd::Exists{
        keys: vec![
            vec!['l' as u8],
            vec!['l' as u8, '2' as u8],
        ],
    }).unwrap();

    assert_eq!(res, 1);
}


#[test]
fn getset() {
    let mut stream = create_stream();

    let res = stream.cmd(proto::cmd::GetSet{
        key: vec!['m' as u8],
        value: vec!['x' as u8],
    }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Nil);

    let res = stream.cmd(proto::cmd::GetSet{
        key: vec!['m' as u8],
        value: vec!['y' as u8],
    }).unwrap();

    assert_eq!(res, proto::cmd::GetReply::Bulk(vec!['x' as u8]));
}


#[test]
fn pubsub_subscribe() {
    let mut stream = create_stream();
    let mut ps = stream.pubsub();

    ps.subscribe(vec![vec!['n' as u8, '1' as u8]]);

    assert_eq!(
        ps.next_message().unwrap(),
        client::PubSubMsg::Subscribe { channel: vec!['n' as u8, '1' as u8], rem: 1 }
    );

    ps.subscribe(vec![vec!['n' as u8, '2' as u8]]);
    assert_eq!(
        ps.next_message().unwrap(),
        client::PubSubMsg::Subscribe { channel: vec!['n' as u8, '2' as u8], rem: 2 }
    );

    ps.unsubscribe(vec![vec!['n' as u8, '1' as u8]]);
    assert_eq!(
        ps.next_message().unwrap(),
        client::PubSubMsg::Unsubscribe { channel: vec!['n' as u8, '1' as u8], rem: 1 }
    );

    ps.unsubscribe(vec![vec!['n' as u8, '2' as u8]]);
    assert_eq!(
        ps.next_message().unwrap(),
        client::PubSubMsg::Unsubscribe { channel: vec!['n' as u8, '2' as u8], rem: 0 }
    );
}


#[test]
fn pubsub_publish_message() {
    let mut stream = create_stream();
    let mut subscriber = stream.pubsub();

    subscriber.subscribe(vec![vec!['o' as u8, '1' as u8]]);
    subscriber.next_message().unwrap();
    subscriber.subscribe(vec![vec!['o' as u8, '2' as u8]]);
    subscriber.next_message().unwrap();

    let mut stream = create_stream();

    stream.cmd(proto::cmd::Publish{
        channel: vec!['o' as u8, '1' as u8],
        message: vec!['w' as u8],
    });
    assert_eq!(
        subscriber.next_message().unwrap(),
        client::PubSubMsg::Message {
            channel: vec!['o' as u8, '1' as u8],
            payload: vec!['w' as u8],
        }
    );

    stream.cmd(proto::cmd::Publish{
        channel: vec!['o' as u8, '2' as u8],
        message: vec!['x' as u8],
    });
    assert_eq!(
        subscriber.next_message().unwrap(),
        client::PubSubMsg::Message {
            channel: vec!['o' as u8, '2' as u8],
            payload: vec!['x' as u8],
        }
    );

    stream.cmd(proto::cmd::Publish{
        channel: vec!['o' as u8, '1' as u8],
        message: vec!['y' as u8],
    });
    assert_eq!(
        subscriber.next_message().unwrap(),
        client::PubSubMsg::Message {
            channel: vec!['o' as u8, '1' as u8],
            payload: vec!['y' as u8],
        }
    );

    stream.cmd(proto::cmd::Publish{
        channel: vec!['o' as u8, '2' as u8],
        message: vec!['z' as u8],
    });
    assert_eq!(
        subscriber.next_message().unwrap(),
        client::PubSubMsg::Message {
            channel: vec!['o' as u8, '2' as u8],
            payload: vec!['z' as u8],
        }
    );
}
