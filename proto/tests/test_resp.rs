extern crate redic_proto as proto;

use proto::cmd::Cmd;
use std::time::Duration;


#[test]
fn encode_get() {
    let mut data = Vec::new();

    proto::cmd::Get{ key: vec!['a' as u8] }.send(&mut data).unwrap();

    assert_eq!(data, "*2\r\n$3\r\nGET\r\n$1\r\na\r\n".as_bytes());
}


#[test]
fn encode_set() {
    let mut data = Vec::new();

    proto::cmd::Set{
        key: vec!['b' as u8],
        value: vec!['x' as u8],
        expiration: None,
        exists: None,
    }.send(&mut data).unwrap();

    assert_eq!(data, "*3\r\n$3\r\nSET\r\n$1\r\nb\r\n$1\r\nx\r\n".as_bytes());
}


#[test]
fn encode_set_px() {
    let mut data = Vec::new();

    proto::cmd::Set{
        key: vec!['c' as u8],
        value: vec!['x' as u8],
        expiration: Some(Duration::new(1, 1_000_000)),
        exists: None,
    }.send(&mut data).unwrap();

    assert_eq!(data, "*5\r\n$3\r\nSET\r\n$1\r\nc\r\n$1\r\nx\r\n$2\r\nPX\r\n$4\r\n1001\r\n".as_bytes());
}
