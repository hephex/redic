use std::io;
use std::time::Duration;
use super::*;

#[derive(Debug)]
pub enum RecvError {
    Server(String),
    Read(ReadError),
    UnexpectedResp,
}

pub trait Cmd {
    type Reply;

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()>;
    fn recv<R: io::BufRead>(&self, reader: &mut R) -> Result<Self::Reply, RecvError>;
}

#[derive(Debug)]
pub struct Append {
    pub key: Bulk,
    pub value: Bulk,
}

impl Cmd for Append {
    type Reply = i64;

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()> {
        Resp::Array(vec![
            Resp::Bulk(vec!['A' as u8, 'P' as u8, 'P' as u8, 'E' as u8, 'N' as u8, 'D' as u8]),
            Resp::Bulk(self.key.clone()),
            Resp::Bulk(self.value.clone()),
        ]).encode(writer)
    }

    fn recv<R: io::BufRead>(&self, reader: &mut R) -> Result<Self::Reply, RecvError> {
        let resp = Resp::decode(reader)
            .map_err(RecvError::Read)?;

        match resp {
            Resp::Integer(i) => Ok(i),
            r @ _ => Err(RecvError::UnexpectedResp),
        }
    }
}

#[derive(Debug)]
pub struct BitCount {
    pub key: Bulk,
    pub range: Option<(i64, i64)>,
}

impl Cmd for BitCount {
    type Reply = i64;

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()> {
        let mut args = vec![
            Resp::Bulk(vec!['B' as u8, 'I' as u8, 'T' as u8, 'C' as u8, 'O' as u8, 'U' as u8, 'N' as u8, 'T' as u8]),
            Resp::Bulk(self.key.clone()),
        ];

        if let Some((start, end)) = self.range {
            args.push(Resp::Bulk(start.to_string().into_bytes()));
            args.push(Resp::Bulk(end.to_string().into_bytes()));
        }

        Resp::Array(args).encode(writer)

    }

    fn recv<R: io::BufRead>(&self, reader: &mut R) -> Result<Self::Reply, RecvError> {
        let resp = Resp::decode(reader)
            .map_err(RecvError::Read)?;

        match resp {
            Resp::Integer(i) => Ok(i),
            r @ _ => Err(RecvError::UnexpectedResp),
        }
    }
}

#[derive(Debug)]
pub struct Exists {
    pub keys: Vec<Bulk>,
}

impl Cmd for Exists {
    type Reply = i64;

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()> {
        let mut args = vec![
            Resp::Bulk(vec!['E' as u8, 'X' as u8, 'I' as u8, 'S' as u8, 'T' as u8, 'S' as u8]),
        ];
        args.extend(self.keys.iter().map(|k| Resp::Bulk(k.clone())));

        Resp::Array(args).encode(writer)

    }

    fn recv<R: io::BufRead>(&self, reader: &mut R) -> Result<Self::Reply, RecvError> {
        let resp = Resp::decode(reader)
            .map_err(RecvError::Read)?;

        match resp {
            Resp::Integer(i) => Ok(i),
            r @ _ => Err(RecvError::UnexpectedResp),
        }
    }
}

#[derive(Debug)]
pub struct Get { pub key: Bulk }

#[derive(Debug, PartialEq, Eq)]
pub enum GetReply {
    Nil,
    Bulk(Bulk),
}

impl Cmd for Get {
    type Reply = GetReply;

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()> {
        Resp::Array(vec![
            Resp::Bulk(vec!['G' as u8, 'E' as u8, 'T' as u8]),
            Resp::Bulk(self.key.clone()),
        ]).encode(writer)
    }

    fn recv<R: io::BufRead>(&self, reader: &mut R) -> Result<Self::Reply, RecvError> {
        let resp = Resp::decode(reader)
            .map_err(RecvError::Read)?;

        match resp {
            Resp::Nil => Ok(GetReply::Nil),
            Resp::Bulk(b) => Ok(GetReply::Bulk(b)),
            Resp::Error(e) => Err(RecvError::Server(e)),
            r @ _ => Err(RecvError::UnexpectedResp),
        }
    }
}

#[derive(Debug)]
pub struct GetSet {
    pub key: Bulk,
    pub value: Bulk,
}


impl Cmd for GetSet {
    type Reply = GetReply;

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()> {
        Resp::Array(vec![
            Resp::Bulk(vec!['G' as u8, 'E' as u8, 'T' as u8, 'S' as u8, 'E' as u8, 'T' as u8]),
            Resp::Bulk(self.key.clone()),
            Resp::Bulk(self.value.clone()),
        ]).encode(writer)
    }

    fn recv<R: io::BufRead>(&self, reader: &mut R) -> Result<Self::Reply, RecvError> {
        let resp = Resp::decode(reader)
            .map_err(RecvError::Read)?;

        match resp {
            Resp::Nil => Ok(GetReply::Nil),
            Resp::Bulk(b) => Ok(GetReply::Bulk(b)),
            Resp::Error(e) => Err(RecvError::Server(e)),
            r @ _ => Err(RecvError::UnexpectedResp),
        }
    }
}

#[derive(Debug)]
pub struct Set {
    pub key: Bulk,
    pub value: Bulk,
    pub expiration: Option<Duration>,
    pub exists: Option<bool>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum SetReply {
    Nil,
    Status(String),
}

impl Cmd for Set {
    type Reply = SetReply;

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()> {
        let mut args = vec![
            Resp::Bulk(vec!['S' as u8, 'E' as u8, 'T' as u8]),
            Resp::Bulk(self.key.clone()),
            Resp::Bulk(self.value.clone()),
        ];

        if let Some(exp) = self.expiration {
            let secs = exp.as_secs();
            let millis = exp.subsec_nanos() as u64 / 1_000_000;

            if millis > 0 {
                args.push(Resp::Bulk(vec!['P' as u8, 'X' as u8]));
                args.push(Resp::Bulk((secs * 1_000 + millis).to_string().into_bytes()));
            } else {
                args.push(Resp::Bulk(vec!['E' as u8, 'X' as u8]));
                args.push(Resp::Bulk(secs.to_string().into_bytes()));
            }
        }

        match self.exists {
            Some(true) => args.push(Resp::Bulk(vec!['X' as u8, 'X' as u8])),
            Some(false) => args.push(Resp::Bulk(vec!['N' as u8, 'X' as u8])),
            _ => {},
        }

        Resp::Array(args).encode(writer)
    }

    fn recv<R: io::BufRead>(&self, reader: &mut R) -> Result<Self::Reply, RecvError> {
        let resp = Resp::decode(reader)
            .map_err(RecvError::Read)?;

        match resp {
            Resp::Nil => Ok(SetReply::Nil),
            Resp::String(s) => Ok(SetReply::Status(s)),
            Resp::Error(e) => Err(RecvError::Server(e)),
            r @ _ => Err(RecvError::UnexpectedResp),
        }
    }
}


#[derive(Debug)]
pub struct Del {
    pub keys: Vec<Bulk>,
}

impl Cmd for Del {
    type Reply = i64;

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()> {
        let mut args = vec![
            Resp::Bulk(vec!['D' as u8, 'E' as u8, 'L' as u8]),
        ];

        args.extend(self.keys.iter().map(|k| Resp::Bulk(k.clone())));

        Resp::Array(args).encode(writer)
    }

    fn recv<R: io::BufRead>(&self, reader: &mut R) -> Result<Self::Reply, RecvError> {
        let resp = Resp::decode(reader)
            .map_err(RecvError::Read)?;

        match resp {
            Resp::Integer(i) => Ok(i),
            r @ _ => Err(RecvError::UnexpectedResp),
        }
    }
}


#[derive(Debug)]
pub struct Publish {
    pub channel: Bulk,
    pub message: Bulk,
}

impl Cmd for Publish {
    type Reply = i64;

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()> {
        Resp::Array(vec![
            Resp::Bulk(vec!['P' as u8, 'U' as u8, 'B' as u8, 'L' as u8, 'I' as u8, 'S' as u8, 'H' as u8]),
            Resp::Bulk(self.channel.clone()),
            Resp::Bulk(self.message.clone()),
        ]).encode(writer)
    }

    fn recv<R: io::BufRead>(&self, reader: &mut R) -> Result<Self::Reply, RecvError> {
        let resp = Resp::decode(reader)
            .map_err(RecvError::Read)?;

        match resp {
            Resp::Integer(i) => Ok(i),
            r @ _ => Err(RecvError::UnexpectedResp),
        }
    }
}


#[derive(Debug)]
pub struct Subscribe {
    pub channels: Vec<Bulk>,
}

impl Cmd for Subscribe {
    type Reply = ();

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()> {
        let mut args = vec![
            Resp::Bulk(vec!['S' as u8, 'U' as u8, 'B' as u8, 'S' as u8, 'C' as u8, 'R' as u8, 'I' as u8, 'B' as u8, 'E' as u8]),
        ];
        args.extend(self.channels.iter().map(|k| Resp::Bulk(k.clone())));

        Resp::Array(args).encode(writer)
    }

    fn recv<R: io::BufRead>(&self, _reader: &mut R) -> Result<Self::Reply, RecvError> {
        return Ok(())
    }
}


#[derive(Debug)]
pub struct Unsubscribe {
    pub channels: Vec<Bulk>,
}

impl Cmd for Unsubscribe {
    type Reply = ();

    fn send<W: io::Write>(&self, writer: W) -> io::Result<()> {
        let mut args = vec![
            Resp::Bulk(vec!['U' as u8, 'N' as u8, 'S' as u8, 'U' as u8, 'B' as u8, 'S' as u8, 'C' as u8, 'R' as u8, 'I' as u8, 'B' as u8, 'E' as u8]),
        ];
        args.extend(self.channels.iter().map(|k| Resp::Bulk(k.clone())));

        Resp::Array(args).encode(writer)
    }

    fn recv<R: io::BufRead>(&self, _reader: &mut R) -> Result<Self::Reply, RecvError> {
        return Ok(())
    }
}
