extern crate itoa;

pub mod cmd;

use std::io;


#[derive(Debug)]
pub enum ReadError {
    IO(io::Error),
    Utf8(std::string::FromUtf8Error),
    ParseInt(std::num::ParseIntError),
    UnexpectedLast(u8),
    UnexpectedType(u8),
    InvalidResp,
}


pub type Bulk = Vec<u8>;

#[derive(Debug)]
pub enum Resp {
    Nil,
    String(String),
    Error(String),
    Integer(i64),
    Bulk(Bulk),
    Array(Vec<Resp>),
}


static CRLF: &'static str = "\r\n";


fn read_line<R>(reader: &mut R, line: &mut Vec<u8>) -> Result<(), ReadError>
    where R: io::BufRead
{
    let mut peek = [0 as u8; 1];

    reader.read_until('\r' as u8, line)
        .map_err(ReadError::IO)?;

    reader.read_exact(&mut peek)
        .map_err(ReadError::IO)?;

    if peek[0] != '\n' as u8 {
        ReadError::UnexpectedLast(peek[0]);
    }

    Ok(())
}


fn parse_int<R>(reader: &mut R) -> Result<i64, ReadError>
    where R: io::BufRead
{
    let mut line = Vec::with_capacity(3);

    read_line(reader, &mut line)?;
    line.pop();

    let line = String::from_utf8(line)
        .map_err(ReadError::Utf8)?;

    line
        .parse()
        .map_err(ReadError::ParseInt)
}


impl Resp {
    pub fn encode<W: io::Write>(&self, mut writer: W) -> io::Result<()> {
        match *self {
            Resp::Nil => {
                writer.write("$-1\r\n".as_bytes())?;
            },
            Resp::String(ref s) => {
                writer.write("+".as_bytes())?;
                writer.write(s.as_bytes())?;
                writer.write(CRLF.as_bytes())?;
            },
            Resp::Error(ref e) => {
                writer.write("-".as_bytes())?;
                writer.write(e.as_bytes())?;
                writer.write(CRLF.as_bytes())?;
            },
            Resp::Integer(ref i) => {
                writer.write(":".as_bytes())?;
                itoa::write(&mut writer, *i)?;
                writer.write(CRLF.as_bytes())?;
            },
            Resp::Bulk(ref b) => {
                writer.write("$".as_bytes())?;
                itoa::write(&mut writer, b.len())?;
                writer.write(CRLF.as_bytes())?;
                writer.write(b.as_slice())?;
                writer.write(CRLF.as_bytes())?;
            },
            Resp::Array(ref a) => {
                writer.write("*".as_bytes())?;
                itoa::write(&mut writer, a.len())?;
                writer.write(CRLF.as_bytes())?;

                let mut data = Vec::with_capacity(4);
                for r in a {
                    r.encode(&mut data)?;
                }

                writer.write(&data)?;
            },
        }

        Ok(())
    }

    pub fn decode<R: io::BufRead>(mut reader: &mut R) -> Result<Resp, ReadError> {
        let mut peek = [0 as u8; 1];

        reader.read_exact(&mut peek)
            .map_err(ReadError::IO)?;

        match peek[0] as char {
            '+' => {
                let mut text = Vec::with_capacity(4);

                read_line(reader, &mut text)?;
                text.pop();

                String::from_utf8(text)
                    .map(Resp::String)
                    .map_err(ReadError::Utf8)
            },
            '-' => {
                let mut text = Vec::with_capacity(4);

                read_line(reader, &mut text)?;
                text.pop();

                String::from_utf8(text)
                    .map(Resp::String)
                    .map_err(ReadError::Utf8)
            },
            ':' => {
                parse_int(&mut reader)
                    .map(|i| Resp::Integer(i))
            },
            '$' => {
                let len = parse_int(&mut reader)?;

                if len < 0 {
                    Ok(Resp::Nil)
                } else {
                    let mut data = vec![0; len as usize];

                    reader.read_exact(&mut data)
                        .map_err(ReadError::IO)?;

                    let mut crlf = [0 as u8; 2];
                    reader.read_exact(&mut crlf)
                        .map_err(ReadError::IO)?;

                    if crlf == ['\r' as u8, '\n' as u8] {
                        Ok(Resp::Bulk(data))
                    } else {
                        Err(ReadError::UnexpectedLast(crlf[0]))
                    }
                }
            },
            '*' => {
                let len = parse_int(&mut reader)?;

                if len < 0 {
                    Ok(Resp::Nil)
                } else {
                    let mut array = Vec::with_capacity(len as usize);

                    for _ in 0..len {
                        let val =  Self::decode(reader)?;

                        array.push(val);
                    }

                    Ok(Resp::Array(array))
                }
            },
            c @ _ => Err(ReadError::UnexpectedType(c as u8)),
        }
    }
}
